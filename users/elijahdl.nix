{ config, lib, pkgs, ... }:
with lib;

let secrets = import ../secrets.nix;
in
{
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.extraUsers.elijahdl = {
    description = "Elijah Love";
    group = "elijahdl";
    extraGroups = [
      "audio"
      "kvm"
      "libvirtd"
      "networkmanager"
      "users"
      "vboxusers"
      "video"
      "wheel"
      "wireshark"
    ];

    uid = 11011;

    createHome = true;
    home = "/home/elijahdl";
    shell = "/run/current-system/sw/bin/zsh";

    openssh.authorizedKeys.keys = secrets.sshKeys.elijahdl;

  };
  users.extraGroups.elijahdl.gid = 11011;

  system.activationScripts =
  {
    # Configure various dotfiles.
    dotfiles = stringAfter [ "users" ]
    ''
      cd /home/elijahdl
    '';
  };
}

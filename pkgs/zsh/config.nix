{ config, pkgs, ... }:

{
  programs.zsh = {
    enable = true;
    ohMyZsh = {
      enable = true;
      theme = "ys";
      plugins = [ "git" "sudo" "wd" "autojump" "common-aliases" "dirhistory" "vagrant" ];
    };
  };

  #environment.variables = {
  #  OH_MY_ZSH = [ "${pkgs.oh-my-zsh}/share/oh-my-zsh" ];
  #};
}

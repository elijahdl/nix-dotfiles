{ config, pkgs, ... }:

let
  hostname = "nixy";
in

{
  imports =
    [
      ../hardware/kvm-enc-zfs.nix


      ../profiles/audio.nix
      #../profiles/bluetooth.nix
      ../profiles/desktop.nix
      #../profiles/docker.nix
      ../profiles/drawing.nix
      #../profiles/email.nix
      ../profiles/games.nix
      ../profiles/graphics.nix
      ../profiles/latex.nix
      ../profiles/networking.nix
      ../profiles/powermanagement.nix
      ../profiles/printing.nix
      ../profiles/rust.nix
      ../profiles/steam.nix
      ../profiles/virtualization.nix
      ../services/xrdp.nix
    ];

#  networking.firewall.enable = false;
 networking.firewall.allowPing = true;
# Set a static IP when I didn't want to set up dhcp
#  networking.defaultGateway = "192.168.0.1";
#  networking.interfaces.enp3s0 = {
#    ipAddress = "192.168.0.42";
#    prefixLength = 24;
#  };

  # Speed up development at the cost of possible build race conditions
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # ZFS options
  boot.supportedFilesystems = ["zfs" ];
  boot.kernelParams = ["zfs_force=1"];
  boot.zfs.forceImportRoot = false;
  boot.zfs.forceImportAll = false;
  networking.hostId = "93af1bc4";

  fileSystems."/" =
    { device = "tank/root";
      fsType = "zfs";
    };

  fileSystems."/etc" =
    { device = "tank/etc";
      fsType = "zfs";
    };

  fileSystems."/tmp" =
    { device = "tank/tmp";
      fsType = "zfs";
    };

  fileSystems."/nix" =
    { device = "tank/nix";
      fsType = "zfs";
    };

  fileSystems."/home" =
    { device = "tank/home";
      fsType = "zfs";
    };

  fileSystems."/srv" =
    { device = "tank/srv";
      fsType = "zfs";
    };

  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/787C-69AB";
      fsType = "vfat";
    };

  swapDevices =
    [ { device = "/dev/zd0"; }
    ];

  boot.initrd.luks.devices = [
    {
      name = "vgcrypt";
      device = "/dev/disk/by-uuid/9b993755-495c-41bb-bb3c-749328ae0450";
      preLVM = true;
      allowDiscards = true;
    }
  ];

  boot.extraModprobeConfig = ''
  options resume=/dev/rootvg/swap
  '';

  
  networking.hostName = "${hostname}";


  powerManagement.enable = true;

  #hardware.bluetooth.enable = true;

}


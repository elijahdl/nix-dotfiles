{ config, pkgs, ... }:

let
  hostname = "journey";
in

{
  imports =
    [


      ../profiles/audio.nix
      ../profiles/bluetooth.nix
      ../profiles/desktop.nix
      ../profiles/docker.nix
      ../profiles/drawing.nix
      #../profiles/email.nix
      ../profiles/games.nix
      ../profiles/graphics.nix
      ../profiles/latex.nix
      ../profiles/networking.nix
      ../profiles/powermanagement.nix
      ../profiles/printing.nix
      ../profiles/rust.nix
      ../profiles/steam.nix
      ../profiles/virtualization.nix
      ../services/xrdp.nix
    ];

#  networking.firewall.enable = false;
 networking.firewall.allowPing = true;
# Set a static IP when I didn't want to set up dhcp
#  networking.defaultGateway = "192.168.0.1";
#  networking.interfaces.enp3s0 = {
#    ipAddress = "192.168.0.42";
#    prefixLength = 24;
#  };

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # ZFS options
  boot.supportedFilesystems = ["zfs" ];
  boot.kernelParams = ["zfs_force=1"];
  boot.zfs.forceImportRoot = false;
  boot.zfs.forceImportAll = false;
  networking.hostId = "93af1bc4";

  boot.initrd.luks.devices = [
    {
      name = "vgcrypt";
      device = "/dev/disk/by-uuid/9b993755-495c-41bb-bb3c-749328ae0450";
      preLVM = true;
      allowDiscards = true;
    }
  ];

  boot.extraModprobeConfig = ''
  options resume=/dev/rootvg/swap
  '';

  
  networking.hostName = "${hostname}";


  powerManagement.enable = true;

  #hardware.bluetooth.enable = true;

}


{ config, pkgs, ... }:

{
  services.xrdp.enable = true;
  services.xrdp.defaultWindowManager = "${pkgs.plasma-desktop}/bin/plasma-desktop";
  networking.firewall.allowedTCPPorts = [ 3389 ];
}

{ config, pkgs, ... }:

{
  imports = [
    ./fstrim.nix
    ./mosh.nix
    ./ntp.nix
    ./sshd.nix
  ];
}

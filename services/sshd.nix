{ config, pkgs, ... }:

{
  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;

    permitRootLogin = "no";

    passwordAuthentication = true;
    challengeResponseAuthentication = false;
  };
}


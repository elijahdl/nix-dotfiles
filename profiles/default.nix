{ config, pkgs, ... }:

{

  imports = [
    ../pkgs/bash/config.nix
    ../pkgs/zsh/config.nix
  ];

  # Enable zsh as a login shell
  programs.zsh.enable = true;

  # List packages installed in system profile. To search by name, run:
  # nix-env -qaP | grep wget
  environment.systemPackages = with pkgs; [
    ack
    apg # random password generation
    aria2
    aspell
    aspellDicts.en
    autojump
    bc
    bind  # nslookup, dig
    blackbox # safely store secrets in git
    bridge-utils
    bsdgames
    ctags
    curl
    diceware
    dos2unix
    dotnetPackages.MaxMindGeoIP2
    duplicity
    efibootmgr
    elinks
    entr
    exfat-utils
    expect
    ffmpeg-full
    file
    fortune
    fzf
    git-lfs
    gitAndTools.gitFull
    gnumake
    gnupg
    gotop
    gparted
    gptfdisk
    gzip
    htop
    idutils
    iftop
    iotop
    lf
    links
    logrotate
    lsof
    lynx
    mkpasswd
    mosh
    ncdu
    ncftp
    neofetch
    neovim
    nfs-utils
    nix-prefetch-scripts
    nmap
    nox # https://github.com/madjar/nox
    openssl
    p7zip
    pandoc
    pass
    pass-otp
    pciutils
    pciutils
    pmutils
    powershell
    psmisc
    pywal
    ranger
    rclone
    ripgrep
    rsync
    sshfs 
    stdenv
    strace
    sudo
    sysstat
    tcpdump
    tmux
    todo-txt-cli
    tree
    unzip
    usbutils
    w3m
    wget
    youtube-dl
    zip
  ];

  environment.shellAliases = {
    "vim" = "${pkgs.neovim}/bin/nvim";
  };

}

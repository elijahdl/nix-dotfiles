{ config, lib, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    krita
    inkscape
  ];

  services.xserver.wacom.enable = true;
}
